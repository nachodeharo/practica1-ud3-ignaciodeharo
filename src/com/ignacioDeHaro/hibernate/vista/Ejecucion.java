package com.ignacioDeHaro.hibernate.vista;

public class Ejecucion {
    /**
     * Trabajo de hibernate con previo mapeo de 5 clases (sin incluir gerente por falta de tiempo) y con tablas intermedias.
     * La modificación personal es que no uso Listar, si no que están siempre actualizados después de añadir/moficiar/eliminar
     * @author Nacho De Haro / SrWolfMoon
     * @see com.ignacioDeHaro.hibernate.clases.Actividades, DetalleProveedor, Gerentes, Instructores, Material, Proveedores, Socios, SociosGerentes
     * @see Controlador
     * @see Modelo
     * @see Vista
     * @version 24/11/2020   1.0
     * @since 1.8
     *
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);
    }
}
